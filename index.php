<?php
require './vendor/autoload.php';

use League\Plates\Engine;

// vars
$baseUrl = './';

// create Plates instance
$templates = new Engine('./templates');

// Get the requested page from the URL, default to "home"
$page = ltrim($_SERVER['REQUEST_URI'], '/');
if (empty($page)) {
    $page = 'home';
}

// render
try {
    echo $templates->render($page, [
        'baseUrl' => $baseUrl
    ]);
} catch (Exception $e) {
    // not found render 404
    http_response_code(404);

    echo $templates->render('404', [
        'baseUrl' => $baseUrl
    ]);
}
