<?php $this->layout('layout', [
    'pageTitle' => 'About',
    'baseUrl' => $baseUrl
]) ?>

<?php $this->start('page_content') ?>
    <!-- simple h1 title component -->
    <?php $this->insert('components/title', [
        'text' => 'About Us'
    ]) ?>

    <!-- some dummy content -->
    <p>Welcome to the about page.</p>
    <a href='/'>Home</a>
<?php $this->stop() ?>
