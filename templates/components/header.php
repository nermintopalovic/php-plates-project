<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->e($pageTitle) ?></title>
    <link rel="stylesheet" href="<?= $this->e($baseUrl) ?>src/styles/main.css">
</head>
