<!DOCTYPE html>
<html lang="en">
<?php $this->insert('components/header', [
    'pageTitle' => $pageTitle,
    'baseUrl' => $baseUrl
]) ?>
<body>
    <?= $this->section('page_content') ?>

    <?php $this->insert('components/footer', [
        'baseUrl' => $baseUrl
    ]) ?>
</body>
</html>
