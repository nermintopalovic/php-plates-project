<?php $this->layout("layout", [
    'pageTitle' => 'Home',
    'baseUrl' => $baseUrl
]) ?>

<?php $this->start('page_content') ?>
    <!-- simple h1 title component -->
    <?php $this->insert('components/title', [
        'text' => 'Homepage'
    ]) ?>

    <!-- some dummy content -->
    <p>Welcome to the homepage.</p>
    <a href='/about'>About</a>

    <!-- rest api data fetch testing -->
    <br>
    <?php
        // $url = 'https://cms.rise2.studio/wp-json/wp/v2/pages/17';
        // $response = file_get_contents($url);

        // if ($response !== FALSE) {
        //     // Decode JSON response to an associative array
        //     $data = json_decode($response, true);

        //     // Print the data
        //     print_r($data);
        // } else {
        //     echo 'Failed to fetch data.';
        // }
    ?>
<?php $this->stop() ?>


