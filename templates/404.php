<?php $this->layout('layout', [
    'pageTitle' => 'Page not found.',
    'baseUrl' => $baseUrl
]) ?>

<?php $this->start('page_content') ?>
    <!-- simple h1 title component -->
    <?php $this->insert('components/title', [
        'text' => '404'
    ]) ?>

    <!-- some dummy content -->
    <p>Page not found.</p>
    <a href='/'>Return Home</a>
<?php $this->stop() ?>
